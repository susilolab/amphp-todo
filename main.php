<?php
declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use Amp\ByteStream\IteratorStream;
use Amp\ByteStream\ResourceOutputStream;
use Amp\Http\Server\{
    Request,
    RequestHandler\CallableRequestHandler,
    Response,
    Router,
    Server,
};
use Amp\Http\Server\StaticContent\DocumentRoot;
use Amp\Http\Status;
use Amp\Log\{ConsoleFormatter, StreamHandler};
use Amp\Socket;
use Amp\Mysql\ConnectionConfig;
use Monolog\Logger;
use Dotenv\Dotenv;
use function Amp\File\openFile;

final class App
{
    public function __construct()
    {
        $dotenv = Dotenv::createImmutable(__DIR__, '.env');
        $dotenv->safeLoad();
    }

    public function run()
    {
        \Amp\Loop::run([$this, 'main']);
    }

    public function main()
    {
        $servers = [
            Socket\listen('0.0.0.0:3000'),
            Socket\listen('[::]:3000'),
        ];

        $logHandler = new StreamHandler(new ResourceOutputStream(\STDOUT));
        $logHandler->setFormatter(new ConsoleFormatter());
        $logger = new Logger('server');
        $logger->pushHandler($logHandler);
        
        $documentRoot = new DocumentRoot(__DIR__ . '/public');
        $pool = $this->getDb();
        $router = new Router();
        $router->setFallback($documentRoot);
        $router->addRoute('GET', '/', new CallableRequestHandler(function () {
            $fs = yield openFile('views/index.html', 'r');
            $body = yield $fs->read();
            return new Response(Status::OK, ['Content-Type' => 'text/html'], $body);
        }));

        $router->addRoute('GET', '/todos/{limit:\+d}', new CallableRequestHandler(function (Request $request) use ($pool) {
            $args = $request->getAttribute(Router::class);
            $limit = (int) $args['limit'];
            echo $limit, PHP_EOL;
            if ($limit < 1) {
                $limit = 20;
            }

            $stmt = yield $pool->prepare('SELECT * FROM todos LIMIT :limit');
            $result = yield $stmt->execute(['limit' => $limit]);
            $resp = [];
            while (yield $result->advance()) {
                $row = $result->getCurrent();
                $resp[] = $row;
            }

            return new Response(Status::OK, ['Content-Type' => 'application/json'], json_encode($resp));
        }));

        $router->addRoute('POST', '/todos', new CallableRequestHandler(function (Request $request) use ($pool) {
            $resp = $request->getBody();

            return new Response(Status::OK, ['Content-Type' => 'application/json'], json_encode($resp));
        }));

        $router->addRoute('GET', '/todos-stream', new CallableRequestHandler(function (Request $request) use ($pool) {
            $args = $request->getAttribute(Router::class);

            $stmt = yield $pool->prepare('SELECT * FROM todos');
            $result = yield $stmt->execute([]);
            $resp = [];

            return new Response(Status::OK, ['Content-Type' => 'application/json'], 
                new IteratorStream(
                    new \Amp\Producer(function (callable $emit) use ($result) {
                        while (yield $result->advance()) {
                            yield $emit(json_encode($result->getCurrent()));
                        }
                    }
            )));
        }));

        $router->addRoute('GET', '/{name}', new CallableRequestHandler(function (Request $request) {
            $args = $request->getAttribute(Router::class);
            return new Response(Status::OK, ['Content-Type' => 'text/plain'], "Hello, {$args['name']}!");
        }));

        $server = new Server($servers, $router, $logger);
        yield $server->start();

        Amp\Loop::onSignal(SIGINT, function (string $watcherId) use ($server) {
            Amp\Loop::cancel($watcherId);
            yield $server->stop();
        });
    }

    public function getDb()
    {
        $connStr = 'host={host} user={user} password={password} db={dbname}';
        $dsn = strtr($connStr, [
            '{host}' => $_SERVER['DB_HOST'],
            '{user}' => $_SERVER['DB_USER'],
            '{password}' => $_SERVER['DB_PASSWD'],
            '{dbname}' => $_SERVER['DB_NAME']
        ]);
        $config = ConnectionConfig::fromString($dsn);

        return Amp\Mysql\pool($config);
    }
}

(new App())->run();

