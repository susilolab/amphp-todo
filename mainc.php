<?php
declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

use Amp\ByteStream;
use Amp\Http\Server\{
    Request,
    RequestHandler\CallableRequestHandler,
    Response,
    Router,
    Server,
};
use Amp\Cluster\Cluster;
use Amp\Http\Status;
use Amp\Log\{ConsoleFormatter, StreamHandler};
use Amp\Mysql\ConnectionConfig;
use Amp\Promise;
use Monolog\Logger;
use Dotenv\Dotenv;
use function Amp\File\openFile;

final class App
{
    public function __construct()
    {
        $dotenv = Dotenv::createImmutable(__DIR__, '.env');
        $dotenv->safeLoad();
    }

    public function run()
    {
        \Amp\Loop::run([$this, 'main']);
    }

    public function main()
    {
        $servers = yield [
            Cluster::listen('0.0.0.0:3000'),
            Cluster::listen('[::]:3000'),
        ];
    
        if (Cluster::isWorker()) {
            $logHandler = Cluster::createLogHandler();
        } else {
            $logHandler = new StreamHandler(ByteStream\getStdout());
            $logHandler->setFormatter(new ConsoleFormatter());
        }

        $logger = new Logger('cluster-' . Cluster::getId());
        $logger->pushHandler($logHandler);
        
        $pool = $this->getDb();
        $router = new Router();
        $router->addRoute('GET', '/', new CallableRequestHandler(function () {
            $fs = yield openFile('views/index.html', 'r');
            $body = yield $fs->read();
            return new Response(Status::OK, ['Content-Type' => 'text/html'], $body);
        }));

        $router->addRoute('GET', '/todos', new CallableRequestHandler(function (Request $request) use ($pool) {
            $args = $request->getAttribute(Router::class);

            $stmt = yield $pool->prepare('SELECT * FROM todos');
            $result = yield $stmt->execute([]);
            $resp = [];
            while (yield $result->advance()) {
                $row = $result->getCurrent();
                $resp[] = $row;
            }

            return new Response(Status::OK, ['Content-Type' => 'application/json'], json_encode($resp));
        }));

        $router->addRoute('GET', '/{name}', new CallableRequestHandler(function (Request $request) {
            $args = $request->getAttribute(Router::class);
            return new Response(Status::OK, ['Content-Type' => 'text/plain'], "Hello, {$args['name']}!");
        }));

        $server = new Server($servers, $router, $logger);
        yield $server->start();

        Cluster::onTerminate(function () use ($server): Promise {
            return $server->stop();
        });
    }

    public function getDb()
    {
        $connStr = 'host={host} user={user} password={password} db={dbname}';
        $dsn = strtr($connStr, [
            '{host}' => $_SERVER['DB_HOST'],
            '{user}' => $_SERVER['DB_USER'],
            '{password}' => $_SERVER['DB_PASSWD'],
            '{dbname}' => $_SERVER['DB_NAME']
        ]);
        $config = ConnectionConfig::fromString($dsn);

        return Amp\Mysql\pool($config);
    }
}

(new App())->run();

