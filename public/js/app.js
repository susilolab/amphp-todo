function saveTodo(body) {
  fetch('/todos', {})
    .then(resp => resp.json())
    .then(resp => {
      const el = document.querySelector('#list-todos')
      resp.map(item => {
        el.innerHTML += `<li>${item.title}</li>`
      })
    })
    .catch(err => console.log(err))
}

document.addEventListener('DOMContentLoaded', () => {
  fetch('/todos', {})
    .then(resp => resp.json())
    .then(resp => {
      const el = document.querySelector('#list-todos')
      resp.map(item => {
        el.innerHTML += `<li>${item.title}</li>`
      })
    })
    .catch(err => console.log(err))

  const inputEl = document.querySelector('input[name="inputText"]')
  inputEl.addEventListener('keydown', (e) => {
    if (e.keyCode === 13) {
      console.log(e.target.value)
      e.target.value = ''
    }
  })
})

